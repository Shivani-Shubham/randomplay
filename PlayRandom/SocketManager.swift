//
//  SocketManager.swift
//  PlayRandom
//
//  Created by SHIVANI DUBEY on 21/11/18.
//  Copyright © 2018 SHIVANI DUBEY. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    static let manager = SocketManager(socketURL: URL(string: "https://ios-test.us-east-1.elasticbeanstalk.com/")!)
    static let socket = manager.defaultSocket
    
    
    class func connectSocket() {
        socket.on("capture") {data, ack in
            debugPrint(data)
            //            guard let cur = data[0] as? Int else { return }
            //
            //            socket.emitWithAck("capture", cur).timingOut(after: 4) {data in
            //                //socket.emit("update", with: ["new data": cur + 2.50])
            //            }
            //
            ack.with("Got your currentAmount", "dude")
        }
//        let token = UserDefaults.standard.getAccessToken()
//
//        self.manager.config = SocketIOClientConfiguration(
//            arrayLiteral: .connectParams(["token": token]), .secure(true) // problem is here in passing token value
//        )
        socket.connect()
    }
    
    class func receiveMsg() {
        socket.on("new message here") { (dataArray, ack) in
            
            print(dataArray.count)
            
        }
    }
}
