//
//  ViewController.swift
//  PlayRandom
//
//  Created by SHIVANI DUBEY on 20/11/18.
//  Copyright © 2018 SHIVANI DUBEY. All rights reserved.
//

import UIKit
import Charts
import UserNotifications


class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var lineChartView: LineChartView!
    var lineChartEntry = [ChartDataEntry]()
    var valueColors = [NSUIColor]()
    var randomNumbers : [Double] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = UserDefaults.standard.randomData {
            randomNumbers = data
        } else {
            randomNumbers = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.randomGenerator), userInfo: nil, repeats: true)
    }
    @objc func randomGenerator() {
        let num = Int.random(in: 0 ... 10)
        let numInDouble = Double(num)
        randomNumbers.append(numInDouble)
        UserDefaults.standard.randomData = randomNumbers
        if let prev = UserDefaults.standard.previousValue {
            if prev == numInDouble {
                triggerNotification()
            }
        }
        UserDefaults.standard.previousValue = Double(num)
        lineChartView.clearValues()
        lineChartView.clear()
        updateGraph()
    }
    func updateGraph() {
        if randomNumbers.count > 0 {
            var count = randomNumbers.count - 1
            for i in 1..<10 {
                let val = randomNumbers[count]
                let value = ChartDataEntry(x: val, y:Double(i))
                count = count - 1
                lineChartEntry.append(value)
                valueColors.append(colorPicker(value: val))
            }
            let dataSet = LineChartDataSet(values: lineChartEntry, label: "Number")
            
            dataSet.colors = valueColors
            dataSet.mode = .cubicBezier
            
            let data = LineChartData()
            data.addDataSet(dataSet)
            lineChartView.data = data
            lineChartView.chartDescription?.text = "My random number chart"
        }
    }
    
    func colorPicker(value : Double) -> NSUIColor {
        if value > 7 {
            return NSUIColor.blue
        }
        else {
            return NSUIColor.black
        }
    }
    
    @objc func triggerNotification(){
        let content = UNMutableNotificationContent()
        let requestIdentifier = "playRandomNotification"
        content.badge = 1
        content.title = NSString.localizedUserNotificationString(forKey: "Random:", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Same number appeared twice", arguments: nil)
        content.sound = UNNotificationSound.default
        //content.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
        content.categoryIdentifier = "playRandom.localNotification"
        // Deliver the notification in five seconds.
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 30.0, repeats: false)
        let request = UNNotificationRequest.init(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().delegate = self
        // Schedule the notification.
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error:Error?) in
            if error != nil {
                print(error?.localizedDescription ?? "error...")
            }
            print("Notification Register Success")
        }
    }
    
    func stopNotification(_ sender: AnyObject) {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
}

