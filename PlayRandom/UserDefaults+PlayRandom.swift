//
//  UserDefaults+PlayRandom.swift
//  PlayRandom
//
//  Created by SHIVANI DUBEY on 23/11/18.
//  Copyright © 2018 SHIVANI DUBEY. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    fileprivate struct UserDefaultsKeys {
        static let RandomDataKey = "RandomData"
        static let PreviousValueKey = "PreviousValue"
    }
    
    var previousValue: Double? {
        get {
            if let data = value(forKey: UserDefaultsKeys.PreviousValueKey) as? Double {
                return data
            }
            return nil
        } set {
            if let data = newValue {
                self.set(data, forKey: UserDefaultsKeys.PreviousValueKey)
            }
            else {
                self.removeObject(forKey: UserDefaultsKeys.PreviousValueKey)
            }
            self.synchronize()
        }
    }
    var  randomData: [Double]? {
        get {
            if let data = value(forKey: UserDefaultsKeys.RandomDataKey) as? [Double] {
                return data
            }
            return nil
        }
        set {
            if let data = newValue {
                self.set(data, forKey: UserDefaultsKeys.RandomDataKey)
            }
            else {
                self.removeObject(forKey: UserDefaultsKeys.RandomDataKey)
            }
            self.synchronize()
        }
    }
}
